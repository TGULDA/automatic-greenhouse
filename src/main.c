/**
  ******************************************************************************
  * @file    main.c
  * @author  Tomas Gulda and Petr Klement, Brno University of Technology, Czechia

  * @date    Dec 1, 2018
  * @brief   The program is used to automatically control the indoor greenhouse.
  * @note    The application controls the lights, the pump and display with external sensors.
             How input is used the temperature and humidity sensor DHT12, the ground humidity
             sensor --- and RTC SENZOR DS3231. How output is used 2.5-6V pump, UV LEDs and
             display with controller HD44780.
  ******************************************************************************
  */

#define F_CPU 16000000UL //define frequency of CPU

/* Includes ------------------------------------------------------------------*/
#include <avr/io.h>
#include <util/delay.h>
#include "lcd.h"
#include "twi.h"
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdint.h>
#include "lcd_symbols.h"

/* Constants and macros ------------------------------------------------------*/
/**
  * @brief Define hours
  */
#define H 0x21 // Set hours
/**
  * @brief Define minutes
  */
#define M 0x59 // Set minutes

/* Function prototypes -------------------------------------------------------*/

void setup(void);                   /* Initialize PORTB, PORTC, ADC, TWI,Timer/Counter0 and Timer/Counter1. */
void set_time(void);                /* Set defined time to Sensor DS3231. */
void set_lcd(void);                 /* Initialize a print to display. */
void dht12_twi_scanner(void);       /* Communication with DHT12 and print measured data to display. */
void ds3231_twi_scanner(void);      /* Communication with DS3231 and print measured data to display. */
void pump_control(void);            /* Pump control function and soil moisture state*/
void light_control(void);           /* Light control function */



/* Global variables ----------------------------------------------------------*/

char uart_string[3];                /* Help variable for print to display */
volatile uint8_t hours;             /* Time in hours from DS3231 */
uint8_t ds3231_address = 0x68;      /* TWI address of sensor DS3231 */
uint8_t twi_status;                 /* Status of TWI communication */
uint16_t pump_adc = 0;              /* Result of ADC interrupt */
volatile uint8_t pump_time = 0;     /* Help variable for pump control */
volatile uint8_t wait_time = 0;     /* Help variable for pump control */
volatile uint8_t write_time=0;      /* Help variable for print data of pump control function to display */




/* Global variable (DHT12 sensor)----------------------------------------------*/
/**
  * @brief States for DHT12 twi communication
  */
typedef enum {
    TEMP_STATE = 1,
    TEMP_READ,
    HUMIDITY_STATE,
    HUMIDTY_READ,
    ERROR_DHT12_STATE


} dht12;
/* FSM for scanning TWI bus from DHT12 ant print to display*/
dht12 dht12_state = TEMP_STATE;

/* Global variable (DS3231 sensor)---------------------------------------------*/
/**
  * @brief States for DS3231 twi communication
  */
typedef enum {
    TIME_WRITE = 1,
    TIME_DATA_READ,
    TIME_ERROR

} ds3231;
/* FSM for scanning TWI bus from DS3231 and print to display*/
ds3231 ds3231_state = TIME_WRITE;

/**
  * @brief States for pump control
  */
typedef enum {
    FULL_STATE = 1,
    THREE_QUARTERS,
    HALF_STATE,
    LOW_STATE,
    PUMP_ON,
    PUMP_REPEAT,
    WAIT,

} pump_st;

pump_st pump_state = FULL_STATE;


/* Main function */
int main(void)
{
    setup();                                           /* Initialize PORTB, PORTC, ADC, TWI,Timer/Counter0 and Timer/Counter1. */
    set_time();                                        /* Set defined time to Sensor DS3231. */
    set_lcd();                                         /* Initialize a print to display. */

    sei();                                             /* Set global interrupt */


    /* Forever loop*/
    while(1)
    {

        light_control();                               /* Set lights */
        if(write_time>3)
        {
           pump_control();
           write_time=0;
        }

    }

    return 0;
}


/* Timer/Counter0 overflow vector */
ISR(TIMER0_OVF_vect)
{

       ds3231_twi_scanner();                /* Call function ds3231_twi_scanner */
       write_time++;


}

/* Timer/Counter1 overflow vector */
ISR(TIMER1_OVF_vect)
{
    pump_time++;
    wait_time++;
    dht12_twi_scanner();                     /* Call function dht12_twi_scanner */


}

/* Completion ADC transfer vector */
ISR(ADC_vect)
{

    pump_adc = ADC;                          /* restult of ADC to pump_adc */

}

/**
  * @brief Setup all peripherals.
  */
void setup(void)
{
    /* Set PORTC and PORTB values */
    DDRC &= ~_BV(PC0);
    DDRB |= _BV(PB2) | _BV(PB3);
    PORTB |= _BV(PB2) ;
    PORTB |= _BV(PB3);

    /* Set ADC */
    ADMUX |= _BV(REFS0);
    ADMUX &= ~(_BV(REFS1)+_BV(MUX0)+_BV(MUX1)+_BV(MUX2)+_BV(MUX3));                 /* Internal Vref turned off, input Channel ADC0 */
    ADCSRA |= _BV(ADEN)|_BV(ADATE)|_BV(ADIE)|_BV(ADPS0)|_BV(ADPS1)|_BV(ADPS2);      /* Enables the ADC,  Auto triggering of the ADC is enabled, when ADC conversion
                                                                                       complete interrupt is activated and division factor is 128 */
    ADCSRB |= _BV(ADTS2) | _BV(ADTS1);                                              /* Trigger source is Timer/Counter1 overflow */

    /* Set Timer/Counter1 */
    TCCR1B |= _BV(CS12);                                                            /* Set pre-separator for Timer/Counter1 to 8 */
    TIMSK1 |= _BV(TOIE1);                                                           /* Enable interrupt for Timer/Counter1 */

     /* Set Timer/Counter0 */
    TCCR0B |= _BV(CS02) | _BV(CS00);                                                /* Set pre-separator for Timer/Counter0 to 1024 */
    TIMSK0 |= _BV(TOIE0);                                                           /* Enable interrupt for Timer/Counter0 */


    /* TWI initialization */
    twi_init();
}


/**
  * @brief Setup the time that is specified in the macro (H, M).
  */
void set_time(void)
{
    ds3231_address = 0x68;                                                           /* Address of DS3231 sensor */
    twi_status = twi_start((ds3231_address<<1) + TWI_WRITE);

        if(twi_status == 0)
        {
            twi_write(0x00);                                                        /* Master address register with seconds in slave */
            twi_write(0x00);                                                        /* Fill in seconds register */
            twi_write(M);                                                           /* Writing defined macro to minutes register */
            twi_write(H);                                                           /* Writing defined macro to hours register */
            twi_stop();
        }
}

/**
  * @brief Setup the LCD layout.
  */
void set_lcd(void)
{
    uint8_t i = 0;
    lcd_init(LCD_DISP_ON);                                                          /* Initialize LCD display */
    lcd_command(1<<LCD_CGRAM);                                                      /* Set address 0 in CGRAM */

    for (i = 0; i < (8*6); i++)
    {
        lcd_data(lcd_symbols[i]);                                                   /* Print defined symbols to LCD display */
    }

    lcd_clrscr();
    lcd_puts("TIME:");                                                              /* Print to display */
    lcd_gotoxy(7,0);
    lcd_puts(":");
    lcd_gotoxy(0,1);
    lcd_puts("t:");
    lcd_gotoxy(4,1);
    lcd_putc(0xdf);
    lcd_puts("C ");
    lcd_putc(0x00);
    lcd_puts(":");
    lcd_gotoxy(11,1);
    lcd_putc('%');
    lcd_gotoxy(11,0);
    lcd_puts("SM:");

}

/**
  * @brief TWI communication with dht12 temperature/humidity sensor
  * @param The communication result is displayed on the LCD
  */
/* Communication with DHT12 and print measured data to display. */
void dht12_twi_scanner(void)
{
    int8_t temperature;              /* Value of temperature */
    uint8_t humidity;                 /* Value of humidity */
    uint8_t dht12_address = 0x5c;                                                   /* TWI address of sensor DHT12 */
    switch (dht12_state)
    {
        case TEMP_STATE:                                                            /* Is done if dht12_state is TEMP_STATE */


            twi_status = twi_start((dht12_address<<1) + TWI_WRITE);                 /* Master send address packet with write bit and check whether slave send answer */

            if(twi_status == 0)                                                     /* Is done if twi_status is 0 (slave answer) */
            {
                twi_write(0x02);                                                    /* Master address register with temperature in slave */
                twi_stop();                                                         /* Stop of TWI communication */
                dht12_state = TEMP_READ;                                            /* Variable dht12_state is TEMP_READ */
            }
            else
                dht12_state = ERROR_DHT12_STATE;                                    /* Is done if twi_status is 1 */
        break;


        case TEMP_READ:                                                             /* State for reading temperature from DHT12 */

            twi_status = twi_start((dht12_address<<1) + TWI_READ);                  /* Master send address packet with read bit and check whether answer send slave */
            if(twi_status == 0)                                                     /* Is done if twi_status is 0 (slave answer) */
            {
                temperature = twi_read_ack();                                       /* Loading temperature from sensor DHT12 */
                twi_stop();                                                         /* Stop of TWI communication */
                itoa(temperature, uart_string,10);                                  /* Type transfer */
                lcd_gotoxy(2,1);                                                    /* Set positions on display*/
                lcd_puts(uart_string);                                              /* Put data of temperature to display */
                dht12_state = HUMIDITY_STATE;                                       /* Variable dht12_state is HUMIDITY_STATE */
            }
            else
                dht12_state = ERROR_DHT12_STATE;                                    /* Is done if twi_status is 1 */
        break;


        case HUMIDITY_STATE:                                                        /* State for loading temperature from DHT12 */

            twi_status = twi_start((dht12_address<<1) + TWI_WRITE);                 /* Master send address packet with write bit and check whether answer send slave */

            if(twi_status == 0)                                                     /* Is done if twi_status is 0 (slave answer) */
            {
                twi_write(0x00);                                                    /* Master address register with humidity in slave */
                twi_stop();                                                         /* Stop of TWI communication */
                dht12_state = HUMIDTY_READ;                                         /* Variable dht12_state is HUMIDITY_READ */

            }
            else
                dht12_state = ERROR_DHT12_STATE;                                    /* Is done if twi_status is 1 */
        break;

        case HUMIDTY_READ:                                                          /* State for reading temperature from DHT12 */

            twi_status = twi_start((dht12_address<<1) + TWI_READ);                  /* Master send address packet with read bit and check whether answer send slave */
            if(twi_status == 0)                                                     /* Is done if twi_status is 0 (slave answer) */
            {
                humidity = twi_read_ack();                                          /* Loading humidity from sensor DHT12 */
                twi_stop();                                                         /* Stop of TWI communication */
                itoa(humidity, uart_string, 10);                                    /* Type transfer */
                lcd_gotoxy(9,1);                                                    /* Set positions on display*/
                lcd_puts(uart_string);                                              /* Put data of humidity to display */
                dht12_state = TEMP_STATE;                                           /* Variable dht12_state is TEMP_STATE */

            }
            else
                dht12_state = ERROR_DHT12_STATE;                                    /* Is done if twi_status is 1 */
        break;

        case ERROR_DHT12_STATE:                                                     /* State for communication error */

            lcd_gotoxy(2,1);
            lcd_puts("--");
            lcd_gotoxy(9,1);
            lcd_puts("--");

            dht12_state = TEMP_STATE;
        break;

        default:
            dht12_state = TEMP_STATE;
    }
}

/**
  * @brief TWI communication with ds3231 RTC
  * @param The communication result is displayed on the LCD
  */
/* Communication with DS3231 and print measured data to display. */
void ds3231_twi_scanner(void)
{
    uint8_t minutes;                /* Time in minutes from DS3231 */
    uint8_t minute_units;               /* Units of minutes */
    uint8_t minute_tens;                /* Tens of minutes */
    uint8_t hour_tens;                  /* Tens of hours */
    uint8_t hour_units;                 /* Units of hours */


    switch (ds3231_state)
    {
        case TIME_WRITE:

            twi_status = twi_start((ds3231_address<<1) + TWI_WRITE);                /* Master send address packet with write bit and check whether slave send answer */

            if(twi_status == 0)
            {
                twi_write(0x01);                                                    /* Master address register with minutes in slave */
                twi_stop();
                ds3231_state = TIME_DATA_READ;
            }
            else
            {
            ds3231_state = TIME_ERROR;
            }
        break;

        case TIME_DATA_READ:
            twi_status = twi_start((ds3231_address<<1) + TWI_READ);                 /* Master send address packet with read bit and check whether answer send slave */

            if(twi_status == 0)
            {
                minutes = twi_read_ack();                                           /* Loading minutes from sensor DS3231 */
                hours = twi_read_nack();                                            /* Loading hours from sensor DS3231 */
                twi_stop();

                minute_tens = (minutes>>4);                                         /* Bit shift variable with tens of minutes */
                itoa(minute_tens, uart_string, 10);
                lcd_gotoxy(8,0);
                lcd_puts(uart_string);

                minute_units = (minutes<<4);                                        /* Bit shift variable with units of minutes */
                minute_units = (minute_units>>4);
                itoa(minute_units, uart_string, 10);
                lcd_gotoxy(9,0);
                lcd_puts(uart_string);

                hour_tens = (hours>>4);                                             /* Bit shift variable with tens of hours */
                itoa(hour_tens, uart_string, 10);
                lcd_gotoxy(5,0);
                lcd_puts(uart_string);

                hour_units = (hours<<4);                                            /* Bit shift variable with units of hours */
                hour_units = (hour_units>>4);
                itoa(hour_units, uart_string, 10);
                lcd_gotoxy(6,0);
                lcd_puts(uart_string);

                ds3231_state = TIME_WRITE;
            }
            else
                ds3231_state = TIME_ERROR;

        break;

        case TIME_ERROR:                                                            /* State for communication error */

            lcd_gotoxy(5,0);
            lcd_puts("--");
            lcd_gotoxy(8,0);
            lcd_puts("--");

            ds3231_state = TIME_WRITE;
            break;

        default:
        ds3231_state = TIME_WRITE;
    }
}


/**
  * @brief Prints the state of soil moisture on the LCD, or starts irrigation.
  * @param The function shows the level of soil moisture on the LCD and controls the pump watering
  */
void pump_control(void)
{
    switch (pump_state)
    {
        case FULL_STATE:                                    /* Print full state character print on LCD */
            if(pump_adc <= 318)
            {
                lcd_gotoxy(14,0);
                lcd_putc(0x02);

            }
            else pump_state = THREE_QUARTERS;

        break;

        case THREE_QUARTERS:                                /* Three quarters status character print on LCD */
            if(pump_adc >= 319 && pump_adc <= 347)
            {
                lcd_gotoxy(14,0);
                lcd_putc(0x03);

            }
            else if(pump_adc < 319)
            {
                pump_state = FULL_STATE;
            }
            else pump_state = HALF_STATE;

        break;

        case HALF_STATE:                                    /* Half status character print on LCD */
            if(pump_adc >= 348 && pump_adc <= 376)
            {
                lcd_gotoxy(14,0);
                lcd_putc(0x04);

            }
            else if(pump_adc < 348)
            {
                pump_state = THREE_QUARTERS;
            }
            else pump_state = LOW_STATE;

        break;

        case LOW_STATE:                                     /* Low status character print on LCD */
            if(pump_adc >= 377 && pump_adc <= 400)
            {
                lcd_gotoxy(14,0);
                lcd_putc(0x05);

            }
            else if(pump_adc < 377)
            {
                pump_state = HALF_STATE;
            }
            else pump_state = PUMP_ON;

        break;

        case PUMP_ON:

            if(bit_is_set(PORTB,3))                         /* If bit is set on PB3 - PUMP on for 2 second */
               {
                PORTB &= ~_BV(PB3);
                pump_time = 0;

               }
            if(bit_is_clear(PORTB,3) && pump_time>=2)       /* If bit is clear on PB3 and pump_time = 2 - PUMP off */
            {
                PORTB |= _BV(PB3);
                pump_state = WAIT;
                wait_time=0;
            }

        break;

        case WAIT:

            if(pump_adc < 318)                              /* wait until pump_adc < 318 or wait_time > 240 */
            {
                pump_state = FULL_STATE;
            }
            else if (wait_time > 240 && pump_adc > 318)
            {
                pump_state = PUMP_REPEAT;
            }
            else
            {
                lcd_gotoxy(14,0);                           /*print on LCD wait status character */
                lcd_putc(0x01);
                pump_state = WAIT;
            }


        break;

        case PUMP_REPEAT:                                   /* repeat state of pump on, pump is turned on for 1 second*/

            if(bit_is_set(PORTB,3))
               {
                PORTB &= ~_BV(PB3);
                pump_time = 0;

               }
            if(bit_is_clear(PORTB,3) && pump_time>=1)
            {
                PORTB |= _BV(PB3);
                pump_state = WAIT;
                wait_time=0;
            }

        break;

        default: pump_state = FULL_STATE;

    }

}

/**
  * @brief Turn on or off lights.
  * @param When time is less than 22 hours and more than 8 hours the lights are turned on.
  */
void light_control(void)
{

        if(hours >= 0x08 && hours <0x22)                /* set lights on */
        {
            PORTB |= _BV(PB2);

        }
        else PORTB &= ~_BV(PB2);                    /*set lights off */

}

